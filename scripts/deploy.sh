#!/bin/bash

# any future command that fails will exit the script
set -e

# Lets write the public key of ASW instance
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# disable the host key checking.
./scripts/hostKeyChecking.sh

# DEPLOYER_SERVER env
DEPLOY_SERVERS=$DEPLOY_SERVERS

# ${string//substring/replacement}
# replacing ","  with nothing.
ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

# Iterate over array and ssh into each EC2 instance and run update.sh
for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  ssh ubuntu@${server} 'bash' < ./deploy/update.sh
done
